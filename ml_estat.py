# -*- coding: utf-8 -*-
"""
Bibliotecas de Funções de Estatística Básica em Python3 puro, refatoradas e
organizadas a partir do cadernos do Professor Dr. Ronaldo F. Ramos, do IFCE,
durante o curso de Machine Leaning na Prática, do IRACEMA Digital.

Fortaleza, 17 de julho de 2019

Por: Eleudson Queiroz
"""

def media(lista):
    """
    Retorna a Médias Aritmética de uma lista de números.        
    """
    return (sum(lista) / len(lista))
               

def mediana(lista):
    """
    Retorna a Mediana de uma lista de números.        
    """
    mediana = None
    if lista:
        n = len(lista)
        hl = sorted(lista)
        if n == 1:
            mediana = hl[0]
        else:
            if n%2 == 0:
                i = int(n/2) - 1
                mediana = (hl[i] + hl[i+1]) / 2
            else:
                i = int((n+1) / 2)
                mediana = hl[i]
    return mediana


def variancia(lista):
    """
    Retorna a Variância de uma lista de números.        
    """
    variancia = None
    if lista:
        n = len(lista)
        soma_desvios_quad = 0
        for i in lista:
            soma_desvios_quad += (i - media(lista))**2
            
        variancia = soma_desvios_quad / n
    return variancia


def desvp(lista):
    """
    Retorna o Desvio Padrão de uma lista de números.        
    """
    return variancia(lista)**0.5 # raiz quadrada


