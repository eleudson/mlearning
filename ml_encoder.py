#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bibliotecas de Funções de Label Encoder em Python3 puro, refatoradas e
organizadas a partir do cadernos do Professor Dr. Ronaldo F. Ramos, do IFCE,
durante o curso de Machine Leaning na Prática, do IRACEMA Digital.

Fortaleza, 17 de julho de 2019

Por: Eleudson Queiroz


*** PARA ENTENDER OS EXEMPLOS ABAIXO CONSIDERE:
    dadosBrutos = [['esferico','sim','alaranjado','rugosa','nao','LARANJA'],
                   ['esferico','nao','vermelho','lisa','nao','MACA'],
                   ['esferico','sim','alaranjado','rugosa','sim','TANGERINA']]
"""

from copy import deepcopy


#---- Label Encoder

def codificar(valor_atual, valores_possiveis):
    """
    Codifica numericamente palavras de acordo com o índice em que
    a mesma se localiza numa lista ordenada sem elementos duplicados.
    
    Exemplo:
        codificar('esferico',['esferico','oval','alongado'])
        
    Saída:
        1
    """
    s = sorted(set(valores_possiveis))
    return s.index(valor_atual)


def cria_dominios(ds):
    """
    Retorna uma lista de listas ordenadas e não duplicada de valores dos 
    atributos de um conjunto de dados.
    
    Obs.: Atributos do exemplo: Formato, Fruta Cítrica, Cor, Rugosidade, Cheiro
    
    Entrada:
    cria_dominios(dadosBrutos)
    
    Saída:
        [['alaranjado', 'vermelho'], ['esferico'], ['lisa', 'rugosa'], 
         ['nao', 'sim']]
    """
    doms = []
    if ds:
        n  = len(ds[0]) - 1 
        m  = len(ds)
        for i in range(n):
            s = set()
            for j in range(m):
                nome = ds[j][i]
                if isinstance(nome, str):
                    s.add(nome)
            l = sorted(s)
            if not l in doms:
                doms.append(l)
    return sorted(doms)


def esta_em_dominio(nome, doms):
    """
    Retorna a lista do domínio a que pertence a palavra ou None se não
    encontrar.
    
    Exemplo:
      esta_em_dominio('alaranjado', [['alaranjado', 'vermelho'],['esferico'], 
                                     ['lisa', 'rugosa'], ['nao', 'sim']])
    Saída:
      ['alaranjado', 'vermelho']
    """
    for i in doms:
        if nome in i:
            return i
    return None


def busca_atributos(nome, ds):
    """
    Retorna uma lista com os atributos de um objeto cujo nome é passado, no
    dataset também informado.
    
    Obs.: O dataset já deve estar transformado.
    
    Exemplo:
        busca_atributo('maca', [[0, 1, 0, 1, 0, 'LARANJA'],
                                [0, 0, 1, 0, 0, 'MACA'],
                                [0, 1, 0, 1, 1, 'TANGERINA']])
    Saída:
        [0, 0, 1, 0, 0]
    """
    for obj in ds:
        if obj[-1].lower() == nome.lower():
            return obj[:-1]
    return None


def label_encoding(ds):
    """
    Retorna o dataset listas de objetos com números substituindo os nomes dos
    valores dos atributos e com nome do objeto na última coluna.
    
    Exemplo:
        # precisa usar o deepcopy para não alterar o dataset
        #passado como parâmetro por referência.
        from copy import deepcopy
        nDs = label_encoding(deepcopy(dadosBrutos))
        
    Saída:
    [[0, 1, 0, 1, 0, 'LARANJA'],
     [0, 0, 1, 0, 0, 'MACA'],
     [0, 1, 0, 1, 1, 'TANGERINA']]
    """
    if ds:
        n = len(ds)
        m = len(ds[0]) - 1
        doms = cria_dominios(ds)
        for i in range(n):
            for j in range(m):
                nome = ds[i][j]
                dominio = esta_em_dominio(nome, doms)
                ds[i][j] = codificar(nome, dominio)
    return ds


#--- One Hot Encoding


def transposta(matriz):
    """
    Retorna uma nova martriz com linhas e colunas invertidas.

    Exemplo:
    dadosBrutos = [['esferico','sim','alaranjado','rugosa','nao','LARANJA'],
                   ['esferico','nao','vermelho','lisa','nao','MACA'],
                   ['esferico','sim','alaranjado','rugosa','sim','TANGERINA']]

        transposta(dadosBrutos)

    Saída:
        
        [['esferico', 'esferico', 'esferico'],
         ['sim', 'nao', 'sim'],
         ['alaranjado', 'vermelho', 'alaranjado'],
         ['rugosa', 'lisa', 'rugosa'],
         ['nao', 'nao', 'sim'],
         ['LARANJA', 'MACA', 'TANGERINA']]
    """
    n = len(matriz)
    m = len(matriz[0])
    nova_matriz = []
    for i in range(m):
        l = []
        for j in range(n):
            l.append(matriz[j][i])
        nova_matriz.append(l)
    return nova_matriz


def novos_rotulos(ds, rots):
    """
    Retorna lista com valores de atributos como cabeçalho da coluna, mesclado
    com nomes predefinidos de colunas booleanas (sim/não).
    
    Exemplo:
        rotulos = ['Formato','Fruta Cítrica','Cor', 'Rugosidade', 'Cheiro']
        novos_rotulos(dadosBrutos, rotulos)

    Saída:
        ['esferico','Fruta Cítrica','vermelho','alaranjado','lisa','rugosa',
         'Cheiro']

    """
    dst = transposta(ds)
    nrots =[]
    for i,j in zip(rots, dst[:-1]):
        nr = set(j)  # tirar duplicados
        if ("sim" in nr) or ("nao" in nr):
            nrots.append(i)
        else:
            for k in nr:
                nrots.append(k)
    return nrots


def one_hot_encoding(ds, rots):
    """
    Retorna uma matriz com rótulos do cabeçalho incluindo valores de atributos
    
    Entrada:
        rotulos = 'Formato','Fruta Cítrica','Cor', 'Rugosidade', 'Cheiro'
        one_hot_encoding(deepcopy(dadosBrutos), rotulos)
        
    Saída:
        (['esferico','Fruta Cítrica','vermelho','alaranjado','lisa','rugosa',
          'Cheiro'],  
         [[1, 1, 0, 1, 0, 1, 0, 'LARANJA'],
          [1, 0, 1, 0, 1, 0, 0, 'MACA'],
          [1, 1, 0, 1, 0, 1, 1, 'TANGERINA']])
    """
    nds = []
    nrots = novos_rotulos(ds, rots)
    
    # zerando atributos
    for i in range(len(ds)):
        l =[]
        for j in range(len(nrots) + 1):  # mais coluna de nome da classe
            l.append(0)
        nds.append(l)
        
    for i in range(len(ds)):
        for j in range(len(ds[0]) - 1): # tira a coluna da classe
            valor = ds[i][j]
            try:
                col = nrots.index(rots[j]) # coluna do nome do atributo
                if valor.lower() == "nao": #converter binarios
                    novo_valor = 0
                elif valor.lower() =="sim":
                    novo_valor = 1
                else:
                    novo_valor = valor  
    
                nds[i][col] = novo_valor
            except(ValueError):
                col = nrots.index(valor)
                nds[i][col] = 1
                
        nds[i][-1] = ds[i][-1] # nome da classe an última semana     
    return nrots, nds


#---- Cálculo das distâncias
    
    
def distancia_euclidiana(nome1, nome2, ds, onehot=False, rotulos=[]):
    """
    Retorna a Distância Euclidiana entre os objetos de nome1 e nome2,
    no dataset informado.
    
    Exemplo:
        distancia_euclidiana('MACA', 'LARANJA', dadosBrutos)
        distancia_euclidiana('maca', 'laranja', dados, True, rotulos)
        
    Saída:
        1.7320508075688772
        2.23606797749979
    """
    if onehot:
        tds = one_hot_encoding(deepcopy(ds), rotulos)[1]
    else:
        tds = label_encoding(deepcopy(ds))

    obj1 = busca_atributos(nome1, tds) 
    obj2 = busca_atributos(nome2, tds) 
    
    if obj1 and obj2:
        d = 0
        for i,j in zip(obj1, obj2):
            d += (i-j)**2
        return d**0.5 # raiz quadrada
    else:
        return None


def distancia_quarteirao(nome1, nome2, ds, onehot=False, rotulos=[]):
    """
    Retorna a Distância Quarteirão ou Manhatan entre os objetos de nome1 e
    nome2, no dataset informado.
    
    Exemplo:
        distancia_quarteirao('MACA', 'LARANJA', dadosBrutos)
        distancia_quarteirao('maca', 'laranja', dados, True, rotulos)
        
    Saída:
        3
        5
    """
    if onehot:
        tds = one_hot_encoding(deepcopy(ds), rotulos)[1]
    else:
        tds = label_encoding(deepcopy(ds))

    obj1 = busca_atributos(nome1, tds) 
    obj2 = busca_atributos(nome2, tds) 
    
    if obj1 and obj2:
        d = 0
        for i,j in zip(obj1, obj2):
            d += abs(i-j)
        return d
    else:
        return None

## teste
#
#dados = [['esferico','sim','alaranjado','rugosa','nao','LARANJA'],
#         ['esferico','nao','vermelho','lisa','nao','MACA'],
#         ['esferico','sim','alaranjado','rugosa','sim','TANGERINA']]
#
#rotulos = ['Formato','Fruta Cítrica','Cor', 'Rugosidade', 'Cheiro']
#
#print('DISTÂNCIA EUCLIDIANA de MACA e LARANJA')
#print('Label Encoding .: ', distancia_euclidiana('maca','laranja',dados))
#print('One Hot Encoding: ', distancia_euclidiana('maca','laranja',dados,True,rotulos))
#
#print('\nDISTÂNCIA QUARTEIRÃO de MACA e LARANJA')
#print('Label Encoding .: ', distancia_quarteirao('maca','laranja',dados))
#print('One Hot Encoding: ', distancia_quarteirao('maca','laranja',dados,True,rotulos))

